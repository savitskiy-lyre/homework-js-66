import React, {useEffect, useMemo, useState} from 'react';
import BackdropPreloader from "../components/UI/BackdropPreloader/BackdropPreloader";

const withLoader = (WrappedComponent, axios) => {
   return (props) => {
      const [loading, setLoading] = useState(false);
      const interceptorsId = useMemo(() => {
         const requestItId = axios.interceptors.request.use((request) => {
            setLoading(true);
            return request;
         })
         const responseItId = axios.interceptors.response.use((response) => {
            setLoading(false);
            return response;
         })
         return [requestItId, responseItId];
      }, []);

      useEffect(() => {
         return () => {
            axios.interceptors.request.eject(interceptorsId[0]);
            axios.interceptors.response.eject(interceptorsId[1]);
         }
      }, [interceptorsId])
      return (
        <>
           {loading && <BackdropPreloader option={{type: Math.floor(Math.random() * 11)}}/>}
           <WrappedComponent {...props}/>
        </>)
   };
};

export default withLoader;
