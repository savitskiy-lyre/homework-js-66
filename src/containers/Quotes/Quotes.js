import React, {useEffect, useState} from 'react';
import {QUOTES_URL} from "../../config";
import Quote from "../../components/Quote/Quote";
import Categories from "../../components/Categories/Categories";
import './Quotes.css';
import withLoader from "../../hoc/withLoader";
import ErrorBoundary from "../../components/ErrorBoundary/ErrorBoundary";
import axiosApi from "../../axiosApi";

const Quotes = ({routeObj, categories}) => {

   const [quotesData, setQuotesData] = useState(null);
   const [onDelete, setOnDelete] = useState(false);
   const currentCategory = categories.reduce((gotIt, category) => {
      if (routeObj.location.pathname === '/quotes/' + category.path) {
         gotIt = category['path']['0'].toUpperCase() + category.path.slice(1, category.path.length)
      }
      return gotIt;
   }, 'All')

   const onQuoteDelete = async (id) => {
      await axiosApi.delete(QUOTES_URL + '/' + id + '.json');
      setOnDelete((prev) => {
         return !prev;
      });
   };

   const onQuoteRedact = (id) => {
      routeObj.history.push(QUOTES_URL + '/' + id + '/edit')
   }
   useEffect(() => {
      const requestQuotes = async () => {
         let plusOption = '';
         for (const category of categories) {
            if (QUOTES_URL + '/' + category.path === routeObj.location.pathname) {
               if (routeObj.location.pathname === QUOTES_URL + '/all') {
                  break;
               }
               plusOption += `?orderBy="category"&equalTo="${category.path}"`;
            }
         }
         const {data} = await axiosApi(QUOTES_URL + '.json' + plusOption);
         setQuotesData(data);
      }
      requestQuotes();
   }, [routeObj, categories, onDelete])

   return (
     <div className="quotes-block">
        <Categories categories={categories}/>
        <div className="quotes-wrapper">
           {quotesData && (
             <div>
                <h4>{currentCategory}</h4>
                <div className="quotes-content">
                   {Object.keys(quotesData).map((quote) => {
                      return (
                        <ErrorBoundary key={quote}>
                           <Quote
                             categories={categories}
                             data={quotesData[quote]}
                             onQuoteDelete={() => onQuoteDelete(quote)}
                             onQuoteRedact={() => onQuoteRedact(quote)}
                           />
                        </ErrorBoundary>)
                   }).reverse()}
                </div>
             </div>
           )}
        </div>
     </div>
   );
};


export default withLoader(Quotes, axiosApi);