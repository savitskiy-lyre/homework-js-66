import React, {useEffect, useState} from 'react';
import {QUOTES_URL} from "../../config";
import './ChangeQuote.scss';
import withLoader from "../../hoc/withLoader";
import axiosApi from "../../axiosApi";

const ChangeQuote = ({routeObj, categories}) => {
   const [categoryVal, setCategoryVal] = useState('all');
   const [authorVal, setAuthorVal] = useState('');
   const [quoteVal, setQuoteVal] = useState('');
   const onQuoteSubmit = async (e) => {
      e.preventDefault();
      const createdData = {
         category: categoryVal,
         author: authorVal,
         content: quoteVal,
      };
      if (routeObj.match.params.id) {
         await axiosApi.put(QUOTES_URL + '/' + routeObj.match.params.id + '.json', createdData);
      } else {
         await axiosApi.post(QUOTES_URL + '.json', createdData);
      }
      routeObj.history.push('/');
   };

   useEffect(() => {
      if (routeObj.match.params.id) {
         const requestQuote = async () => {
            const {data} = await axiosApi.get(QUOTES_URL + '/' + routeObj.match.params.id + '.json')
            setAuthorVal(data.author);
            setQuoteVal(data.content);
            setCategoryVal(data.category);
         }
         requestQuote();
      }

   }, [routeObj])


   return (
     <div className="change-quote-block">
        <form className='change-quote-form' onSubmit={onQuoteSubmit}>
           <div className="label">
              Category
           </div>
           <select value={categoryVal} onChange={(e) => setCategoryVal(e.target.value)}>
              {categories.map((category) => {
                 return <option value={category.path} key={category.id + category.title}>{category.title}</option>;
              })}
           </select>
           <div className="label">
              Author
           </div>
           <input type="text" value={authorVal} onChange={e => setAuthorVal(e.target.value)} required/>
           <div className="label">
              Quote's text
           </div>
           <textarea value={quoteVal} onChange={e => setQuoteVal(e.target.value)} cols="30" rows="10" required/>
           <button className='submit-btn'>Confirm</button>
        </form>
     </div>
   );
};

export default withLoader(ChangeQuote, axiosApi);