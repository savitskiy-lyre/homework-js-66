import React, {Component} from 'react';
import {Preloader, Puff} from "react-preloader-icon";
import axios from "axios";

class ErrorBoundary extends Component {
   state = {
      hasError: false,
      error: null,
      isRequesting: false,
      isAccepted: false
   }

   componentDidUpdate(prevProps, prevState, snapshot) {
      if (!this.state.isRequesting && this.state.hasError) {
         this.setState({isRequesting: true});

         axios.post('https://tutorial-sample-posts-blog-default-rtdb.firebaseio.com/h66/errors.json', {
            name: this.state.error.message,
            data: this.state.error.stack
         }).then(() => {
            this.setState({isAccepted: true})
         })
      }

   }
   componentDidCatch(error, errorInfo) {
      this.setState({hasError: true, error: error});
   }

   render() {
      if (this.state.hasError) {
         return (
           <div style={{margin: '40px', color: 'red', display: 'flex', alignItems: 'center'}}>
              <div style={{marginRight: '10px'}}>
                 Error: {this.state.error.message}
              </div>
              {this.state.isRequesting && !this.state.isAccepted && (
                <Preloader
                  use={Puff}
                  size={34}
                  strokeWidth={10}
                  strokeColor="black"
                  duration={1200}/>
              )}
              {this.state.isRequesting && this.state.isAccepted && (
                <div
                  style={{color: 'green', fontSize: '30px'}}
                  dangerouslySetInnerHTML={{__html: "&#10003"}}/>
              )}
           </div>)
      } else {
         return this.props.children;
      }
   }
}

export default ErrorBoundary;
