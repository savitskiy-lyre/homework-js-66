import React from 'react';
import './Categories.css';
import {NavLink} from "react-router-dom";

const Categories = ({categories}) => {
   const activeCategory = {
      color: 'green',
      textDecoration: 'underline',
   }
   return (
     <div className='categories-block'>
        <h4>Categories :</h4>
        <div className='categories-wrapper'>
           <ul>
              {categories.map((category) => {
                 if (category.path === 'all'){
                    return (
                      <li key={category.id}>
                         <NavLink
                           to={'/quotes/' + category.path}
                           activeStyle={activeCategory}
                           isActive={(match, location) => {
                              return location.pathname === '/' || location.pathname === '/quotes/all';
                           }}
                         >
                            {category.title}
                         </NavLink>
                      </li>
                    );
                 }
                 return (
                   <li key={category.id}>
                      <NavLink
                        to={'/quotes/' + category.path}
                        activeStyle={activeCategory}
                      >
                         {category.title}
                      </NavLink>
                   </li>
                 );
              })}
           </ul>
        </div>
     </div>
   );
};

export default Categories;